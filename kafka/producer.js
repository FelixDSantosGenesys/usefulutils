/**
 * Before running the producer example, run the following commands to prepare Minio:
 *   mkdir -p /var/minio/data/example-schemas
 *   docker run -p 9000:9000 -e "MINIO_ACCESS_KEY=$AWS_ACCESS_KEY_ID" -e "MINIO_SECRET_KEY=$AWS_SECRET_ACCESS_KEY" -v /var/minio/data:/data -v /var/minio/config:/root/.minio minio/minio server /data
 */

const _ = require('lodash');
const uuidv4 = require('uuid/v4');
const { SchemaRegistry, KafkaAvroProducer } = require('@altocloud/node-kafka-client');
const { getSchemaByNameSync, getSchemaHashByName } = require('@genesys/common-events');
const {buildDefaultWebEvent} = require('./fixtures');

const AVRO_NAME = 'com.genesys.journey.WebEvent';

const avroSchema = getSchemaByNameSync(AVRO_NAME);
const avroSchemaHash = getSchemaHashByName(AVRO_NAME);

const event = buildDefaultWebEvent();

const schema = {
  content: avroSchema,
  hash: avroSchemaHash,
};
const schemaRegistry = new SchemaRegistry({
  bucketName: 'avro-schemas',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  endpoint: process.env.S3_ENDPOINT || 'http://localhost:9000',
});
const defaultHeaders = { originPlatform: 'test' };

const producer = KafkaAvroProducer.getProducer(schemaRegistry, schema, defaultHeaders, {
  'client.id': 'my-service/42 (i-1234567890abcdef)',
  'metadata.broker.list': 'localhost:9092',
  'compression.codec': 'snappy',
  'request.required.acks': 1,
});

producer.connect()
  .then(() => {
    console.log('Producer ready!');

    producer.on('event.error', (err) => {
      console.error('event.error', { err });
    });

    // setInterval(() => {
    const organizationId = uuidv4();
    const userId = uuidv4();
    const messageValue = {
      organizationId,
      userId,
      status: _.sample(['online', 'offline', 'ready', 'busy']),
    };
    const messageHeaders = {
      correlationId: uuidv4(),
      orgId: organizationId,
    };
    console.log('Producing: ', event)
    producer.produce(event, userId, messageHeaders)
      .then(() => console.log(`[${new Date().toISOString()}] Message published successfully`))
      .catch(err => console.log(`[${new Date().toISOString()}] Message failed`, { err }));
    // }, 100);
  });
