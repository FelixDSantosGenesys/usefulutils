function buildDefaultWebEvent(overrides) {
  const userAgentString = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36';

  return Object.assign({}, {
    session: {
      id: '8e460a39-865f-4bb6-b67f-4f962777b90c',
      createdDate: Date.now(),
      shortId: '5768',
    },
    page: {
      url: 'http://example.com/#auto-insurance',
      domain: 'example.com',
      fragment: '#auto-insurance',
      hostname: 'example.com',
      pathname: '/',
    },
    browser: {
      family: 'Chrome',
      version: '69.0.3497',
    },
    referrer: {
      url: 'http://example.com',
      domain: 'example.com',
      hostname: 'example.com',
      pathname: '/',
    },
    device: {
      category: 'desktop',
      type: 'Other',
      osFamily: 'Mac OS X',
      osVersion: '10.13.6',
    },
    geolocation: {
      country: 'IE',
      countryName: 'Ireland',
    },
    mktCampaign: {
	source: "facebook",
	name: "my campaign",
	},
    attributes: {
      name: {
        dataType: 'string',
        value: 'myName',
      },
      age: {
        dataType: 'number',
        value: '24',
      },
      isCustomer: {
        dataType: 'boolean',
        value: 'true',
      },
    },
    traits: {
      name: {
        dataType: 'string',
        value: 'myName',
      },
      age: {
        dataType: 'number',
        value: '27',
      },
      email: {
        dataType: 'string',
        value: 'myEmail@email.com',
      },
    },
    id: '44fed646-4228-4bf6-9e1d-4331bcadbcae',
    organizationId: 'd460a77c-9870-404f-9711-4be1cc247b66',
    createdDate: Date.now(),
    customerId: '9b08a272-9638-4f12-8824-36ac70d9fd28',
    eventName: 'custom_integration_action_triggering_event',
    searchQuery: null,
    userAgentString: userAgentString,
    ipAddress: '16.17.18.19',
    loginId: '909f4fe7-6c68-4530-82f6-db45480f32a6',
  }, overrides);
}

module.exports = {
  buildDefaultWebEvent,
};
