const Promise = require('bluebird');
const { SchemaRegistry, KafkaAvroConsumer } = require('@altocloud/node-kafka-client');

const schemaRegistry = new SchemaRegistry({
  bucketName: 'avro-schemas',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  endpoint: process.env.S3_ENDPOINT || 'http://localhost:9000',
});

const consumer = KafkaAvroConsumer.getConsumer(schemaRegistry, {
  'client.id': 'my-service/42 (i-1234567890abcdef)',
  'group.id': 'kafka',
  'metadata.broker.list': 'localhost:9092',
  'statistics.interval.ms': 10000,
});

consumer.connect()
  .then(() => {
    consumer.on('error', err => console.error({ err }));
    consumer.on('event.stats', stats => console.log({ stats }));
    consumer.subscribe(['JourneyWebEvents']);
    consumer.run({ batchSize: 2 }, onMessageSet);
  });

function onMessageSet(messages) {
  return Promise.map(messages, message => console.log({ event: message.decodedValue }));
}
